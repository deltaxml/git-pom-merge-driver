# DeltaXML Git POM Merge Driver (Preview) #

This is the README for the DeltaXML git POM merge driver.  

It allows git to merge maven POM files using DeltaXML's algorithms, with some optimizations for Maven POM files, rather then the internal line based algorithms used by git/diff3.
It is an implementation of the system discussed in the DeltaXML Blog posting:  [Automatic Merge Conflict Resolving for git](https://www.deltaxml.com/blog/xml/automatic-merge-conflict-resolving-for-git/?utm_source=BitBucket&utm_medium=ReadMe&utm_campaign=Git%20Merge%20Driver&utm_term=Git&utm_content=NA)

It depends on and is a sample for the DeltaXML-XML-Merge product which is available separately, please see [our website](https://www.deltaxml.com/products/merge/xml-merge/?utm_source=BitBucket&utm_medium=ReadMe&utm_campaign=Git%20Merge%20Driver&utm_term=Git&utm_content=NA).

## How do I get set up? ##

### Compile and build ###

Please note: this sample depends on XML Merge version 9.0 or later.

#### Building the driver jar ####

The XML-Merge release provides some jar files that are not available in a public maven repository, but
which are included in our XML-Merge release.

In order to install these as local (`$HOME/.m2`) dependencies you must run the POM first with the *initialize* phase.

In order for this to run correctly the POM must be told which version of XML Merge you are using. These properties are defined at the top of the `pom.xml`.  The pom file can be edited or these properties can be specified whilst building. 

The easiest way of building the sample is to clone or unpack this code into the samples subdirectory of the XML-Merge release, for example:

```
pwd
/Users/nigelw/Downloads/DeltaXML-XML-Merge-9_0_0_j/samples
git clone https://bitbucket.org/deltaxml/git-merge-driver.git
```
In this example the version of XML Merge downloaded is 9.0.0 which uses XML Compare 11.0.0, but you may have later versions.
In the downloaded directory you can find the deltaxml jars and the versions which you need to specify.

`deltaxml-X.Y.Z.jar` provides the xml.compare.version, X.Y.Z

`deltaxml-merge-A.B.C.jar` provides the xml.merge.version, A.B.C

```
/usr/local/deltaxml/DeltaXML-XML-Merge-9_0_0_j $ ls -l deltaxml-*.jar
-rw-r--r--  1 nigelw  deltaxml   2287956 21 Jun 12:37 deltaxml-11.0.0.jar
-rw-r--r--  1 nigelw  deltaxml   1281199 21 Jun 12:37 deltaxml-merge-9.0.0.jar
-rw-r--r--  1 nigelw  deltaxml  57172248 21 Jun 12:37 deltaxml-merge-rest-9.0.0.jar
-rw-r--r--  1 nigelw  deltaxml   8327427 21 Jun 12:37 deltaxml-merge-rest-client-9.0.0.jar
```
We can specify these versions on the Maven command line as follows
 
  * `mvn initialize -Dxml.merge.version=9.0.0 -Dxml.compare.version=11.0.0`
 
If you clone or unpack somewhere other than the samples subdirectory it will also be necessary to specify an install.dir property either on the command-line or in `pom.xml`:  
 
  * `mvn initialize -Dxml.merge.version=9.0.0 -Dxml.compare.version=11.0.0 -Dxml.merge.install.dir=/usr/local/deltaxml/DeltaXML-XML-Merge-9_0_0_j`
 
After that one off step you can build the driver with `mvn package` - you still need to specify the versions if you did not edit the POM to set them up;
 
  * `mvn package -Dxml.merge.version=9.0.0 -Dxml.compare.version=11.0.0`
 
### License setup ###

If you are evaluating DeltaXML-Merge or have purchased a subcription you will have a license file
with the filename: `deltaxml-merge.lic`.  A copy of this file should be place in the
directory containing merge-driver jar file.  The maven build should copy this file
to the `target` directory, but if you customize the build or make a permanent installation
you may also need to copy/install the license file.

### Sample data and script ###

The `src/test` directory contains sample data and a shell script to create a repository
structure as a demonstration of the merge driver.

Running the script will create a repository with three branches: master, v1 and v2. 

```
cd .../src/test 
src/test $ ./pom-merge-driver-testcase.sh 
Initialized empty Git repository in /Users/nigelw/dev/git-pom-merge-driver/src/test/.git/
[master (root-commit) 36acbb2] initial commit of .gitattributes and .gitignore
 1 file changed, 1 insertion(+)
 create mode 100644 .gitattributes
[master 558eebe] master POM initial commit
 2 files changed, 30 insertions(+)
 create mode 100644 .gitignore
 create mode 100644 pom.xml
Already on 'master'
Switched to a new branch 'v1'
[v1 16caf88] commit v1 branch POM edits
 1 file changed, 5 insertions(+), 5 deletions(-)
Switched to branch 'master'
Switched to a new branch 'v2'
[v2 be15209] commit v2 branch POM edits
 1 file changed, 6 insertions(+), 6 deletions(-)
Switched to branch 'master'

After repository setup we have the following branches (output of: git branch --list)

* master
  v1
  v2


To merge the feature branches into the master run:
   git merge v1 -m "merge v1 into master"
followed by:
   git merge v2 -m "merge v2 into master"
```

The final lines of the script suggest git commands that should be used merge the v1 and v2 branches back onto master.

These can be run and output like the following should be produced:

```
src/test $ git merge v1 -m "merge v1 into master"
Updating 558eebe..16caf88
Fast-forward (no commit created; -m option ignored)
 pom.xml | 10 +++++-----
 1 file changed, 5 insertions(+), 5 deletions(-)
src/test $ git merge v2 -m "merge v2 into master"
DeltaXML XML Merge Driver: conflicts remain and need resolving for pom.xml
Auto-merging pom.xml
CONFLICT (content): Merge conflict in pom.xml
Automatic merge failed; fix conflicts and then commit the result.
```

Note: If you move the script or the driver jar file to more permanent locations then the new
locations should be used to update the sample script and/or driver script.

### Merge Driver Permanent Setup ###

While the sample above has been configured with an easy test/demonstration of a merge, for other use
cases we would recommend a more permanent and flexible install using absolute file paths.

Copy the provided `src/main/resources/pom-merge-driver.sh` file to a permanent location and adjust the relative path used to refer to the `git-pom-merge-driver` jar file to an absolute path reflecting its permanent install location on the system.

Create `.gitattributes` with patterns in your git repository to associate xml files with the merge drivers.  For example:
```
pom.xml  merge=pommerge
```

Then in git config configure the xml merge driver, using --local, --global or --system as appropriate:
```
$ git config --local merge.pommerge.name "DeltaXML POM Merge"
$ git config --local merge.pommerge.driver "/Users/nigelw/bin/pom-merge-driver.sh %O %A %B %L %P"
```

## POM Input and Output filters

This preview uses XSLT filters for input and output processing of Maven POM files.
These filters optimize the merge processing using an understanding of POM semantics as follows.

### The POM input filter

This filter keys POM dependencies for better alignment.  The keying uses the group and dependency name.  See DeltaXML product documentation for further details of keying.

This is a partial implementation that shows one optimization for POM processing.
A more complete solution would consider other keying and orderless possibilities by a more thorough review of POM syntax and semantics.

### The POM output filter

An output filter is provided that demonstrates how conflicts could be automatically resolved.  The filter is called `pom-dependency-version-resolver.xsl`.  It is **not** configured in the driver by default and the code will need to be updated and recompiled to use it.

#### Example without the version resolver

If you compile the driver directly from this repository the data samples contain a dependency version conflict.
When the driver is run it will signal to git the presence of the conflict and the user will have to resolve the conflict.
At the command line the merge output looks like this:

```
src/test $ git merge v2 -m "merge v2 into master"
DeltaXML XML Merge Driver: conflicts remain and need resolving for pom.xml
Auto-merging pom.xml
CONFLICT (content): Merge conflict in pom.xml
Automatic merge failed; fix conflicts and then commit the result.
```

And the result file will contain the dependency conflicts represented using DeltaXML conflict markup, for example:

```
<dependency deltaxml:key="com.deltaxml.merge:merge" deltaxml:deltaV2="ancestor!=mine!=theirs">
  <groupId deltaxml:deltaV2="ancestor=mine=theirs">com.deltaxml.merge</groupId>
  <artifactId deltaxml:deltaV2="ancestor=mine=theirs">merge</artifactId>
  <version deltaxml:deltaV2="ancestor!=mine!=theirs">
    <deltaxml:textGroup deltaxml:deltaV2="ancestor!=mine!=theirs" deltaxml:edit-type="modify">
      <deltaxml:text deltaxml:deltaV2="ancestor">7.0.4</deltaxml:text>
      <deltaxml:text deltaxml:deltaV2="mine">7.1.0</deltaxml:text>
      <deltaxml:text deltaxml:deltaV2="theirs">7.0.6</deltaxml:text>
    </deltaxml:textGroup>
  </version>
</dependency>
```
#### Example with the version resolver

The sample test data in this preview demonstrates a common software development scenario.  Two branches, perhaps feature branches have updated a dependency independently of one another from the version specified in their ancestor version.
In GUI conflict resolvers users would see a three way conflict and would be able to select from versions known or labelled as *mine*, *thiers*, *local*, *remote* or *ancestor*.  The automatic resolver, expressed as XSLT code, simply selects the
highest version number of the dependency, using maven's three dot separated versioning model.  Selecting the highest
version generally works because dependencies and APIs tend to be upwardly compatible between releases.

To experiment with the version resolver remove the comments associated with the files configuring the merge output filter as follows before recompiling the driver (as discussed above using the `package` command):

```
// comment/uncomment the next four lines to see effect of the version resolver filter
FilterStep pomOutput= twm.getFilterStepHelper().newFilterStepFromResource("pom-dependency-version-resolver.xsl", "pom-output");
FilterChain outputFilters= twm.getFilterStepHelper().newFilterChain();
outputFilters.addStep(pomOutput);
twm.setExtensionPoint(ExtensionPoint.OUTPUT_POST_TABLE, outputFilters);
```

With these changes the test data will not be in conflict after the merge as the version conflicts are automatically resolved.
The user will not be told that conflicts need to be resolved, for example:

```
src/test $ git merge v2 -m "merge v2 into master"
DeltaXML XML Merge Driver: no merge conflicts for pom.xml
Auto-merging pom.xml
Merge made by the 'recursive' strategy.
 pom.xml | 48 +++++++++++++++++++++++-------------------------
 1 file changed, 23 insertions(+), 25 deletions(-)
```

And the result file will not contain any conflict markup (or any other form of DeltaXML markup), for example:

```
<dependency>
  <groupId>com.deltaxml.merge</groupId>
  <artifactId>merge</artifactId>
  <version>7.1.0</version>
</dependency>
```

The automatic resolving functionality we have demonstrated in this preview release is included directly in the git merge driver.
It would also be possible to show the user that there was a version conflict and move the resolving functionality into a *git mergetool* that the user could invoke to resovle the conflicts.  Such as mergetool could incorporate some of the functionality in this preview in that it would run the resolver XSLT code on the result, check for remaining conflicts (there may be other forms of conflict from the merge) and remove/clean-up DeltaXML markup when conflict free.

#### Driver (lack of) configuration

The use of the commented out code in the above examples is not ideal.  It would have been ideal if there were a way of passing a parameter to the merge driver from the git command line, or adjusting the driver behaviour through the git configuration mechanism.  We could not find a workable approach to such parameterisation or configuration.  It would be possible to create two different merge drivers based on the commented/uncommented output filtering code and these could then be driven through different patterns in `.gitattributes`, but since maven pom files are generally named `pom.xml` this doesn't really help in many use cases.

One remaining option would be to develop conventions in your organisation for writing POM files, perhaps using a property element or XML comment in them to indicate whether dependency versions should be automatically resolved.  The output filtering code would always be used but the logic used in the resolver XSLT filter could be adjusted and run according to whether the chosen POM element, property or comment was present.


## Feedback ##

Please use our [support portal](https://docs.deltaxml.com/support/latest/support-channels-13304648.html?utm_source=BitBucket&utm_medium=ReadMe&utm_campaign=Git%20Merge%20Driver&utm_term=Git&utm_content=NA) (Jira Service Desk).


Pull requests for enhancement or fixes are also welcome via bitbucket.

## About DeltaXML ##

We provide flawless XML and JSON comparison and merge solutions to identify, visualise and implement XML and JSON diffs quickly and accurately.
Our products are available as Java API, RESTful API and SaaS, to learn more please [visit our website](
https://www.deltaxml.com/products/?utm_source=BitBucket&utm_medium=ReadMe&utm_campaign=Git%20Merge%20Driver&utm_term=Git&utm_content=NA).
