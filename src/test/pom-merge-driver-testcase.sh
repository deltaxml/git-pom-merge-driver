#! /bin/bash
## https://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within
##// be careful of readlink and symlinks on macOS
SAMPLE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# clear up any previous test runs

rm -rf .git .gitattributes .gitignore
rm -f demo1.xml


git init
echo "pom.xml merge=pommerge" > .gitattributes
echo "data" > .gitignore
echo "merge-driver-testcase.sh" >> .gitignore
git add .gitattributes .gitignore
git commit .gitattributes -m "initial commit of .gitattributes and .gitignore"

git config --local merge.pommerge.name "DeltaXML POM Merge"
git config --local merge.pommerge.driver "$SAMPLE_DIR/../main/resources/pom-merge-driver.sh %O %A %B %L %P"

cp $SAMPLE_DIR/data/demo1/pom-a.xml pom.xml

git add pom.xml
git commit -a -m "master POM initial commit"

git checkout master
git checkout -b v1

cp $SAMPLE_DIR/data/demo1/pom-v1.xml pom.xml

git commit -a -m "commit v1 branch POM edits"

git checkout master
git checkout -b v2

cp $SAMPLE_DIR/data/demo1/pom-v2.xml pom.xml

git commit -a -m "commit v2 branch POM edits"

git checkout master

echo ''
echo "After repository setup we have the following branches (output of: git branch --list)"
echo ''

git branch --list

## git status

##git log --graph --all
echo ''
echo ''
echo "To merge the feature branches into the master run:"
echo '   git merge v1 -m "merge v1 into master"'
echo "followed by:"
echo '   git merge v2 -m "merge v2 into master"'
