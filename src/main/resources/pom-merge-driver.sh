#! /bin/bash

#Parameters
# $1 (%O) ancestor version        temp filename
# $2 (%A) current version         temp filename
# $3 (%B) other branches version  temp filename
# $4 (%L) conflict marker size    positive integer (normally 7 chars)
# $5 (%P) pathname into which the merged result will eventually be stored
#
# A git merge driver is expected to
#   leave the result of the merge in the file named $2 (%A) by overwriting it,
#   exit with status := 0 if it managed to merge them cleanly, or non-zero if there were conflicts.
#

# This driver is a placeholder, it would be possible to invoke the java command directly as the driver, however
# in future it is hoped to add fallback support, so for example, where there is an error (perhaps the
# inputs are not well-formed or invalid) the driver will fallback to using git's internal line based
# algorithm which would be best invoked from a shell script rather than java

if test -s $1 
then
  java -jar ../../target/git-pom-merge-driver-7.0.4.jar $1 $2 $3 $4 $5
else
  # some two way merge cases (for example file not in ancestor commit) have zero size $1
  java -jar ../../target/git-pom-merge-driver-7.0.4.jar $2 $2 $3 $4 $5
fi

javaresult=$?

case $javaresult in
0)
        echo "DeltaXML XML Merge Driver: no merge conflicts for $5"
        cp $5 $2
        ;;
1)
        echo "DeltaXML XML Merge Driver: conflicts remain and need resolving for $5"
        cp $5 $2
        ;;
*)
        echo "DeltaXML XML Merge Driver error"
        ;;
esac
exit $javaresult