<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1"
    xmlns:pom="http://maven.apache.org/POM/4.0.0"
    version="3.0">

    <xsl:output method="xml" indent="yes"/>
    <xsl:mode on-no-match="shallow-copy"/>

    <xsl:template match="pom:dependencies">
      <xsl:copy>
        <xsl:attribute name="deltaxml:ordered" select="'false'"/>
        <xsl:apply-templates select="@*, node()"/>
      </xsl:copy>
    </xsl:template>

    <xsl:template match="pom:dependency">
      <xsl:copy>
        <xsl:attribute name="deltaxml:key" select="concat(pom:groupId, ':', pom:artifactId)"/>            
        <xsl:apply-templates select="@*, node()"/>
      </xsl:copy>
    </xsl:template>

</xsl:stylesheet>