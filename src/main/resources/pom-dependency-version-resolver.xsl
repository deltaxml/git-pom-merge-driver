<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:math="http://www.w3.org/2005/xpath-functions/math"
  xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1"
  xmlns:pom="http://maven.apache.org/POM/4.0.0"
  exclude-result-prefixes="xs math"
  version="3.0">
  <xsl:mode on-no-match="shallow-copy"/>


  <!-- pom versions conflicts will have a version element with a
      nested textGroup.  If WordByWord=false and Splitting=false
      then there should be no other non-whitespace text or
      elements inside the version -->
  <xsl:template match="pom:dependency/pom:version[deltaxml:textGroup]
                [empty(* except deltaxml:textGroup) and empty(text()[normalize-space(.) ne ''])]">
    <xsl:copy>
      <!-- https://stackoverflow.com/questions/40202510/xsl-sort-numbers-separated-by-periods -->
      <!-- the use of last() after sorting means we'll use the highest (semantic) version number -->
      <xsl:sequence select="sort(deltaxml:textGroup/deltaxml:text/text(), (), 
                                 function($t){tokenize($t, '\.')!xs:integer(.)})[last()]"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>